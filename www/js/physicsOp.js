var physicStore = {};

let phys_guid;

function mobileParticleGenOp(tag, joyX, joyY, pX, pY, sPs) {
    let parts = [];
    // sPs is shots per second
    // TODO force one of the values to be 2
    if (joyX > 20 || joyY > 20 || joyX < -20 || joyY < -20) {
        if (!physicStore[tag].hiatus) {
            physicStore[tag].hiatus = true;
            setTimeout(function() {
                physicStore[tag].hiatus = false;
            }, 1000 / sPs);
            phys_guid = guid();
            parts.push(new Particle(
                phys_guid,
                pX,
                pY,
                HALF_UNIT,
                HALF_UNIT,
                joyX / 50,
                joyY / 50,
                function() {}
            ));
        }
    }

    return parts;
}

var finalX, finalY, testX, testY;

function playerCollisionCheck(col_map, map_w, x, y, shiftX, shiftY, speed, progress) {
    finalX = x;
    finalY = y;
    if (shiftX !== 0) {
        finalX = x + ((shiftX / 100) * speed * progress);
        testX = Math.round(finalX);
        testY = Math.round(finalY);
        if (col_map[testX + CLEAN_UNIT + (testY + CLEAN_UNIT) * map_w] !== 0) {
            // top left corner overlaps wall
            finalX = x;
        } else if (col_map[testX + 2 * CLEAN_UNIT + (testY + CLEAN_UNIT) * map_w] !== 0) {
            // top right corner overlaps wall
            finalX = x;
        } else if (col_map[testX + CLEAN_UNIT + (testY + 2 * CLEAN_UNIT) * map_w] !== 0) {
            // bottom left corner overlaps wall
            finalX = x;
        } else if (col_map[testX + 2 * CLEAN_UNIT + (testY + 2 * CLEAN_UNIT) * map_w] !== 0) {
            // bottom right corner overlaps wall
            finalX = x;
        }
    }

    if (shiftY !== 0) {
        finalY = y + ((shiftY / 100) * speed * progress);
        testX = Math.round(finalX);
        testY = Math.round(finalY);
        if (col_map[testX + CLEAN_UNIT + (testY + CLEAN_UNIT) * map_w] !== 0) {
            // top left corner overlaps wall
            finalY = y;
        } else if (col_map[testX + 2 * CLEAN_UNIT + (testY + CLEAN_UNIT) * map_w] !== 0) {
            // top right corner overlaps wall
            finalY = y;
        } else if (col_map[testX + CLEAN_UNIT + (testY + 2 * CLEAN_UNIT) * map_w] !== 0) {
            // bottom left corner overlaps wall
            finalY = y;
        } else if (col_map[testX + 2 * CLEAN_UNIT + (testY + 2 * CLEAN_UNIT) * map_w] !== 0) {
            // bottom right corner overlaps wall
            finalY = y;
        }
    }

    return [finalX, finalY];
}

var part;

function performParticleUpdates(col_map, map_w, particles, progress, mobs) {
    const keys = Object.keys(particles);
    let hitmap = [];
    for (var i = 0; i < keys.length; i++) {
        if (typeof particles[keys[i]] == "undefined") {
            continue;
        }

        part = particles[keys[i]];
        part.update(progress);
        // check if it hits a wall
        if (col_map[part.getX() + CLEAN_UNIT + ((part.getY() + CLEAN_UNIT) * map_w)] !== 0) {
            // top left corner overlaps wall
            hitmap.push(keys[i]);
        } else if (col_map[part.getX() + 2 * CLEAN_UNIT + ((part.getY() + CLEAN_UNIT) * map_w)] !== 0) {
            // top right corner overlaps wall
            hitmap.push(keys[i]);
        } else if (col_map[part.getX() + CLEAN_UNIT + ((part.getY() + 2 * CLEAN_UNIT) * map_w)] !== 0) {
            // bottom left corner overlaps wall
            hitmap.push(keys[i]);
        } else if (col_map[part.getX() + 2 * CLEAN_UNIT + ((part.getY() + 2 * CLEAN_UNIT) * map_w)] !== 0) {
            // bottom right corner overlaps wall
            hitmap.push(keys[i]);
        }
        // check if it hits a mob
        const mobkeys3 = Object.keys(mobs);
        for (var m = 0; m < mobkeys3.length; m++) {
            // for some reason a mobs x and y are it's bottom right coord instead of top left
            // so I subtract height and width to compensate
            if (mobs[mobkeys3[m]].x < part.getX() && part.getX() < mobs[mobkeys3[m]].x + mobs[mobkeys3[m]].w) {
                if (mobs[mobkeys3[m]].y < part.getY() && part.getY() < mobs[mobkeys3[m]].y + mobs[mobkeys3[m]].h) {
                    part.hit(mobs[mobkeys3[m]]);
                    mobs[mobkeys3[m]].hit(part.type);
                }
            }
        }
    }

    return hitmap;
}
