var bootView = new View("boot");
bootView.set('fire_click', function(e) {});
bootView.set('mousemove', function(e) {});
bootView.set('keypress', function(e) {});
bootView.set('keyup', function(e) {});
bootView.set('peer_con', function(e) {});
bootView.set('ws_message', function(e) {});
bootView.set('init', function() {});
bootView.set('pause', function() {});
bootView.set('resume', function() {});
bootView.set('click', function() {});
bootView.set('mouse_click', function() {});
bootView.set('shot_click', function(e) {});
bootView.set('render', function() {});
bootView.set('update', function() {
    if(views["menu"]) {
        bootView.showView("menu");
    }
});
