// TODO recalculate when window rescales
const DEBUG_MODE = true;
const BASE_URL = "localhost:8080";
const MAZE_SIZE = 10;
const CELL_SIZE = $(window).width() / 4;
const UNIT = CELL_SIZE * 0.1;
const CLEAN_UNIT = Math.round(UNIT);
const HALF_UNIT = UNIT / 2;
const CORNER_PADX = $(window).width() / 2 + HALF_UNIT;
const CORNER_PADY = $(window).height() / 2 + HALF_UNIT;
const SIXTY_FPS = 50 / 3;

// particle types
const PARTICLE_NORMAL = 1;

// mob types
const MOB_TYPE_TERRIS = 1;
const MOB_TYPE_WINGED_TERRIS = 2;

// game event controller
const EVENT_HANDLER = new EventEmitter();

// stats
const STATS = new Stats();
const STATS2 = new Stats();

var MOUSE_X = 0;
var MOUSE_Y = 0;

GAME_DATA = {
    MUSIC: {}
};
