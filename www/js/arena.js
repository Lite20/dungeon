var arenaView = new View("arena");
var arenaData = {
    UP: false,
    DOWN: false,
    LEFT: false,
    RIGHT: false,
    prevx: 0,
    prevy: 0,
    player: {
        x: 26,
        y: 26,
        speed: 2,
        life: 100,
        shiftX: 0,
        shiftY: 0
    },
    playerCons: {}
};

arenaData.peerHandle = function (msg) {
    let data = JSON.parse(msg);
    console.log(this);
};

arenaView.set('click', function (e) {});
arenaView.set('mouse_click', function (e) {});
arenaView.set('fire_click', function (e) {});
arenaView.set('mousemove', function (e) {});
arenaView.set('pause', function () {});
arenaView.set('resume', function () {});
arenaView.set('keypress', function (e) {
    switch (e.key) {
        case 'w':
            arenaData.UP = true;
            break;
        case 'a':
            arenaData.LEFT = true;
            break;
        case 's':
            arenaData.DOWN = true;
            break;
        case 'd':
            arenaData.RIGHT = true;
            break;
        default:
            console.log(e.key);
            break;
    }
});

arenaView.set('keyup', function (e) {
    switch (e.key) {
        case 'w':
            arenaData.UP = false;
            break;
        case 'a':
            arenaData.LEFT = false;
            break;
        case 's':
            arenaData.DOWN = false;
            break;
        case 'd':
            arenaData.RIGHT = false;
            break;
        default:
            console.log(e.key);
            break;
    }
});

arenaView.set('ws_message', function (msg) {
    let data = JSON.parse(msg);
    if (data.id == 'identify') {
        if (data.s !== 'suc') {
            console.log("failed to auth with gameapi");
        } else {
            GAME_DATA.queue = data.queue;
            console.log("authed with gameapi");
        }
    } else if (data.id == 'broadcast') {
        GAME_DATA.vote += data.vote;
        for (var i = 0; i < data.buffer.length; i++) {
            if (data.buffer[i].id == 'join') {
                GAME_DATA.queue.push(data.buffer[i].peerid);
                break;
            }
        }
    }
});

arenaView.set('peer_con', function (conn) {
    arenaData.playerCons[GAME_DATA.queue[i]] = conn;
    conn.on('data', arenaData.peerHandle);
});

arenaView.set('init', function () {
    GAME_DATA.MUSIC.menuBackground.stop();
    arenaData.maze = new Maze(MAZE_SIZE, GAME_DATA.SEED);
    arenaData.mapcanvas = renderMaze(arenaData.maze, MAZE_SIZE);
    arenaData.mapctx = arenaData.mapcanvas.getContext('2d');
    arenaData.render = true;
    // connect to players
    for (var i = 0; i < GAME_DATA.queue.length; i++) {
        console.log("Connecting to " + GAME_DATA.queue[i]);
        if (typeof arenaData.playerCons[GAME_DATA.queue[i]] == "undefined") {
            var conn = GAME_DATA.peer.connect(GAME_DATA.queue[i]);
            conn.on('open', function () {
                conn.send('player'); // username
            });

            conn.on('data', arenaData.peerHandle);
        }
    }
});

arenaView.set('draw', function (canvas, ctx) {
    if (!arenaData.render) return;

    arenaData.player.shiftY = 0;
    arenaData.player.shiftX = 0;

    if (arenaData.UP) arenaData.player.shiftY -= 100;
    if (arenaData.DOWN) arenaData.player.shiftY += 100;
    if (arenaData.LEFT) arenaData.player.shiftX -= 100;
    if (arenaData.RIGHT) arenaData.player.shiftX += 100;
    if (joystick2.deltaX() !== 0) arenaData.player.shiftX = joystick2.deltaX();
    if (joystick2.deltaY() !== 0) arenaData.player.shiftY = joystick2.deltaY();

    // TODO optimize by caching imageData
    if (arenaData.player.shiftX !== 0) {
        arenaData.prevx = arenaData.player.x;
        arenaData.player.x += (arenaData.player.shiftX / 100) * arenaData.player.speed;
        if (arenaData.mapctx.getImageData(arenaData.player.x + UNIT, arenaData.player.y + UNIT, 1, 1).data[0] !== 0) {
            // top left corner overlaps wall
            arenaData.player.x = arenaData.prevx;
        } else if (arenaData.mapctx.getImageData(arenaData.player.x + 2 * UNIT, arenaData.player.y + UNIT, 1, 1).data[0] !== 0) {
            // top right corner overlaps wall
            arenaData.player.x = arenaData.prevx;
        } else if (arenaData.mapctx.getImageData(arenaData.player.x + UNIT, arenaData.player.y + 2 * UNIT, 1, 1).data[0] !== 0) {
            // bottom left corner overlaps wall
            arenaData.player.x = arenaData.prevx;
        } else if (arenaData.mapctx.getImageData(arenaData.player.x + 2 * UNIT, arenaData.player.y + 2 * UNIT, 1, 1).data[0] !== 0) {
            // bottom right corner overlaps wall
            arenaData.player.x = arenaData.prevx;
        }
    }

    if (arenaData.player.shiftY !== 0) {
        arenaData.prevy = arenaData.player.y;
        arenaData.player.y += (arenaData.player.shiftY / 100) * arenaData.player.speed;
        if (arenaData.mapctx.getImageData(arenaData.player.x + UNIT, arenaData.player.y + UNIT, 1, 1).data[0] !== 0) {
            // top left corner overlaps wall
            arenaData.player.y = arenaData.prevy;
        } else if (arenaData.mapctx.getImageData(arenaData.player.x + 2 * UNIT, arenaData.player.y + UNIT, 1, 1).data[0] !== 0) {
            // top right corner overlaps wall
            arenaData.player.y = arenaData.prevy;
        } else if (arenaData.mapctx.getImageData(arenaData.player.x + UNIT, arenaData.player.y + 2 * UNIT, 1, 1).data[0] !== 0) {
            // bottom left corner overlaps wall
            arenaData.player.y = arenaData.prevy;
        } else if (arenaData.mapctx.getImageData(arenaData.player.x + 2 * UNIT, arenaData.player.y + 2 * UNIT, 1, 1).data[0] !== 0) {
            // bottom right corner overlaps wall
            arenaData.player.y = arenaData.prevy;
        }
    }

    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    // TODO optimize by caching width and height variables
    ctx.drawImage(
        arenaData.mapcanvas,
        arenaData.player.x - CORNER_PADX,
        arenaData.player.y - CORNER_PADY,
        $(window).width(),
        $(window).height(),
        0,
        0,
        $(window).width(),
        $(window).height()
    );
    // player
    ctx.fillStyle = "#0f0";
    ctx.fillRect(UNIT + CORNER_PADX, UNIT + CORNER_PADY, UNIT, UNIT);
});
