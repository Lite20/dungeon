function initStats() {
    STATS.showPanel(0);
    STATS2.showPanel(0);

    $(STATS.domElement).attr('id', 'stats');
    $(STATS2.domElement).attr('id', 'stats2');

    document.body.appendChild(STATS.domElement);
    document.body.appendChild(STATS2.domElement);
}

function initPeerjs() {
    GAME_DATA.peer = new Peer({
        host: BASE_URL.split(':')[0],
        port: 8880,
        path: "peerjs"
    });

    GAME_DATA.peer.on('open', function (id) {
        GAME_DATA.PEER_ID = id;
        console.log("PEER ID: " + id);
    });

    GAME_DATA.peer.on('connection', function (e) {
        views[view].peer_con(e);
    });
}

function initJoysticks() {
    // right joystick
    var joystick = new VirtualJoystick({
        container: document.getElementById('container'),
        strokeStyle: 'cyan',
        limitStickTravel: true,
        stickRadius: 100
    });

    // left joystick
    var joystick2 = new VirtualJoystick({
        container: document.getElementById('container'),
        strokeStyle: 'orange',
        limitStickTravel: true,
        stickRadius: 100
    });

    // set up events
    joystick.addEventListener('touchStartValidation', (e) => {
        return !(e.changedTouches[0].pageX < window.innerWidth / 2);
    });

    joystick2.addEventListener('touchStartValidation', (e) => {
        return !(event.changedTouches[0].pageX >= window.innerWidth / 2);
    });

    joystick.addEventListener('touchStart', () => {
        EVENT_HANDLER.emit('joystick1');
    });

    joystick2.addEventListener('touchStart', () => {
        EVENT_HANDLER.emit('joystick2');
    });
}

function initCanvas() {
    $("#container").css('position', 'absolute');
    if (typeof process !== "undefined") {
        if (process.platform == "win32") $("#container").remove();
    }

    const canvas = document.getElementById("canvas");
    canvas.width = $(window).width();
    canvas.height = $(window).height();
    const ctx = canvas.getContext('2d');

    $("#canvas").click((e) => {
        EVENT_HANDLER.emit('mouseclick', e);
    });

    canvas.addEventListener('mousemove', (e) => {
        MOUSE_X = e.clientX;
        MOUSE_Y = e.clientY;
    });
}

function initLoops() {
    // TODO leverage web-workers to improve physics
    var index_progress, lastRender = 0;

    function update(timestamp) {
        window.requestAnimationFrame(update);
        STATS2.begin();
        index_progress = (timestamp - lastRender) / SIXTY_FPS;
        views[view].update(index_progress, timestamp);
        lastRender = timestamp;
        STATS2.end();
    }

    function render() {
        window.requestAnimationFrame(render);
        STATS.begin();
        views[view].render(canvas, ctx);
        STATS.end();
    }

    window.requestAnimationFrame(render);
    window.requestAnimationFrame(update);
}

function initKeyInput() {
    document.onkeydown = (e) => {
        EVENT_HANDLER.emit('keydown_' + e.char, e);
    };

    document.onkeyup = (e) => {
        EVENT_HANDLER.emit('keyup_' + e.char, e);
    };
}

function initWebsocket() {
    GAME_DATA.socket = new WebSocket('ws://' + BASE_URL + '/gameapi');
    GAME_DATA.socket.addEventListener('open', (e) => {
        GAME_DATA.SOCKET_ACTIVE = true;
    });

    GAME_DATA.socket.addEventListener('message', (e) => {
        if (typeof views[view].ws_message !== "undefined") views[view].ws_message(e.data);
    });
}

function initConsole() {
    const print = console.log;
    GAME_DATA.console = [];
    console.log = (msg) => {
        print(msg);
        GAME_DATA.console.push(msg);
        if (GAME_DATA.console.length > 10) GAME_DATA.console.shift();
    }
}
