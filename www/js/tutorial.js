var tutView = new View("tutorial");
var tutData = {
    jitterForce: 0,
    jitterX: 0,
    jitterY: 0,
    maze: new Maze(2, "tut"),
    UP: false,
    DOWN: false,
    LEFT: false,
    RIGHT: false,
    stage: 0,
    mobileStage: [
        ["Welcome to Dungeon!", "Let me teach you how to play."],
        ["There are two invisible", "joysticks on your screen."],
        ["Tap and drag on the left", "side of your screen to move."],
        ["Awesome! Walk around a bit", "and get comfortable."],
        ["We use our left thumb,", "but do whatever feels right."],
        ["To shoot, drag your hand on", "the right side of the screen."],
        ["Why don't we give you some", "target practice?"],
        ["Here's a Terris. Aim the right", "joystick at it to shoot."],
        ["That was too easy. How about", "a Winged Terris?"],
        ["Wow! Okay, one last thing", ""],
        ["Tap a chest to open it.", "Here's a chest!"]
    ],
    pcStage: [
        ["Welcome to Dungeon!", "Let me teach you how to play."],
        ["Use the 'W', 'A', 'S', and 'D'", "keys to move around."],
        ["Sweet! Okay, lets show you", "how to eliminate mobs."],
        ["That over there, is a", "Terris"],
        ["You can shoot by pressing", "an arrow key"],
        ["Okay. Now that's not", "always effective."],
        ["You can also shoot by", "clicking with your mouse"],
        ["Here's a Winged Terris.", "Click in it's direction"],
        ["Well done!", ""],
        ["Lastly, let me show you", "how to open chests"],
        ["This chest has amazing gifts.", "Press <E> to open it!"]
    ],
    player: {
        x: 26,
        y: 26,
        speed: 2,
        life: 100,
        shiftX: 0,
        shiftY: 0
    },
    particles: [],
    mobs: []
};

GAME_DATA.MUSIC.tutBackground = new Howl({
    src: ['assets/audio/dopplo - tutorial.mp3'],
    autoplay: false,
    loop: true
});

GAME_DATA.MUSIC.promptPing = new Howl({
    src: ['assets/audio/prompt_ping.mp3'],
    autoplay: false,
    loop: false
});

tutData.mazeWidth = CELL_SIZE * tutData.maze.map.length;
tutData.mapcanvas = renderMaze(tutData.maze, 2);
let tut_oppedMap = optimizeCollsionMap(tutData.mapcanvas);
const tut_opMap = tut_oppedMap.map;
const tut_opMapWidth = tut_oppedMap.w;
tutData.tut_mapctx = tutData.mapcanvas.getContext('2d');
tutView.set('click', function (e) {
    GAME_DATA.MUSIC.promptPing.play();
    tutData.stage++;
    switch (tutData.stage) {
        case 6:
            tut_id = guid();
            tutData.mobs[tut_id] = MobCreateTerris(tut_id, CELL_SIZE * 1.5, CLEAN_UNIT * 4, tut_opMap, tut_opMapWidth, function () {
                delete tutData.mobs[this.id];
            }, "tut");
            break;
        case 7:
            tut_id = guid();
            tutData.mobs[tut_id] = MobCreateWingedTerris(tut_id, CLEAN_UNIT * 3, CELL_SIZE * 1.5, tut_opMap, tut_opMapWidth, function () {
                delete tutData.mobs[this.id];
            }, "tut");
            break;
    }
});

tutView.set('fire_click', function (e) {
    GAME_DATA.MUSIC.promptPing.play();
    tutData.stage++;
    switch (tutData.stage) {
        case 6:
            tut_id = guid();
            tutData.mobs[tut_id] = MobCreateTerris(tut_id, CELL_SIZE * 1.5, CLEAN_UNIT * 4, tut_opMap, tut_opMapWidth, function () {
                delete tutData.mobs[this.id];
            }, "tut");
            break;
        case 7:
            tut_id = guid();
            tutData.mobs[tut_id] = MobCreateWingedTerris(tut_id, CLEAN_UNIT * 3, CELL_SIZE * 1.5, tut_opMap, tut_opMapWidth, function () {
                delete tutData.mobs[this.id];
            }, "tut");
            break;
    }
});

tutView.set('peer_con', function (e) {});
tutView.set('ws_message', function (e) {});
tutView.set('shot_click', function (e) {});
tutView.set('mousemove', function (e) {
    tutData.mouseX = e.clientX;
    tutData.mouseY = e.clientY;
});

var tut_guid2;
var tut_hiatus2 = false;
// TODO improve accuracy
tutView.set('mouse_click', function (e) {
    var x = e.clientX - (canvas.width / 2);
    var y = e.clientY - (canvas.height / 2);
    if (x > 100) {
        let oldx = x;
        x = 100;
        y = y * (100 / oldx);
    }

    if (x < -100) {
        let oldx = x;
        x = -100;
        y = y * (-100 / oldx);
    }

    if (y > 100) {
        let oldy = y;
        y = 100;
        x = x * (100 / oldy);
    }

    if (y < -100) {
        let oldy = y;
        y = -100;
        x = x * (-100 / oldy);
    }

    if (!tut_hiatus2) {
        tut_hiatus2 = true;
        setTimeout(function () {
            tut_hiatus2 = false;
        }, 1000 / tutData.shotsPerSecond);
        tut_guid2 = guid();
        tutData.particles[tut_guid2] = new Particle(
            tut_guid2,
            tutData.player.x,
            tutData.player.y,
            HALF_UNIT,
            HALF_UNIT,
            x / 50,
            y / 50,
            PARTICLE_NORMAL,
            function () {
                delete tutData.particles[this.id];
            }
        );
    }
});

let tut_id;
tutView.set('keypress', function (e) {
    switch (e.keyCode) {
        case 87:
            tutData.UP = true;
            break;
        case 65:
            tutData.LEFT = true;
            break;
        case 83:
            tutData.DOWN = true;
            break;
        case 68:
            tutData.RIGHT = true;
            break;
        case 13:
            GAME_DATA.MUSIC.promptPing.play();
            tutData.stage++;
            switch (tutData.stage) {
                case 3:
                    tut_id = guid();
                    tutData.mobs[tut_id] = MobCreateTerris(tut_id, CELL_SIZE * 1.5, CLEAN_UNIT * 4, tut_opMap, tut_opMapWidth, function () {
                        delete tutData.mobs[this.id];
                    }, "tut");
                    break;
                case 7:
                    tut_id = guid();
                    tutData.mobs[tut_id] = MobCreateWingedTerris(tut_id, CLEAN_UNIT * 3, CELL_SIZE * 1.5, tut_opMap, tut_opMapWidth, function () {
                        delete tutData.mobs[this.id];
                    }, "tut");
                    break;
            }

            break;
        case 37:
            tut_id = guid();
            tutData.particles[tut_id] = new Particle(
                tut_id,
                tutData.player.x,
                tutData.player.y,
                HALF_UNIT,
                HALF_UNIT, -2, 0,
                PARTICLE_NORMAL,
                function () {
                    delete tutData.particles[this.id];
                }
            );

            break;
        case 38:
            tut_id = guid();
            tutData.particles[tut_id] = new Particle(
                tut_id,
                tutData.player.x,
                tutData.player.y,
                HALF_UNIT,
                HALF_UNIT,
                0, -2,
                PARTICLE_NORMAL,
                function () {
                    delete tutData.particles[this.id];
                }
            );

            break;
        case 39:
            tut_id = guid();
            tutData.particles[tut_id] = new Particle(
                tut_id,
                tutData.player.x,
                tutData.player.y,
                HALF_UNIT,
                HALF_UNIT,
                2, 0,
                PARTICLE_NORMAL,
                function () {
                    delete tutData.particles[this.id];
                }
            );

            break;
        case 40:
            tut_id = guid();
            tutData.particles[tut_id] = new Particle(
                tut_id,
                tutData.player.x,
                tutData.player.y,
                HALF_UNIT,
                HALF_UNIT,
                0, 2,
                PARTICLE_NORMAL,
                function () {
                    delete tutData.particles[this.id];
                }
            );

            break;
        default:
            console.log(e.keyCode);
            break;
    }
});

tutView.set('keyup', function (e) {
    switch (e.keyCode) {
        case 87:
            tutData.UP = false;
            break;
        case 65:
            tutData.LEFT = false;
            break;
        case 83:
            tutData.DOWN = false;
            break;
        case 68:
            tutData.RIGHT = false;
            break;
        default:
            console.log(e.keyCode);
            break;
    }
});

tutView.set('pause', GAME_DATA.MUSIC.tutBackground.pause);
tutView.set('resume', GAME_DATA.MUSIC.tutBackground.play);
tutView.set('init', function () {
    GAME_DATA.MUSIC[menuData.selectedBackground].stop();
    GAME_DATA.MUSIC.tutBackground.play();
    tutData.stages = tutData.pcStage;
    if (typeof platform !== "undefined") {
        if (platform.android) tutData.stages = tutData.mobileStage;
    }
});

let tut_prevx, tut_prevy, tut_guid;
let tut_mapctx = tutData.mapcanvas.getContext('2d');
let tut_tick = 0;
tutView.set('update', function (progress, timestamp) {
    tutData.jitterX = Math.round(Math.random() * tutData.jitterForce);
    tutData.jitterY = Math.round(Math.random() * tutData.jitterForce);
    tut_tick++;
    if (tut_tick > 1) tut_tick = 0;
    // TODO only do if mobile
    // perform particle creation operation for mobile devices
    let mPGOResult = mobileParticleGenOp(
        "tut",
        joystick.deltaX(),
        joystick.deltaY(),
        tutData.player.x,
        tutData.player.y,
        tutData.shotsPerSecond
    );

    // iterate over result creating particles requested by the physics operation
    for (var i = 0; i < mPGOResult.length; i++) {
        mPGOResult[i].hit = function () {
            delete tutData.particles[this.id];
        };

        tutData.particles[mPGOResult[i].id] = mPGOResult[i];
    }

    // check if the player moved in any direction
    tutData.player.shiftY = 0;
    tutData.player.shiftX = 0;
    if (tutData.UP) {
        tutData.player.shiftY -= 100;
    }

    if (tutData.DOWN) {
        tutData.player.shiftY += 100;
    }

    if (tutData.LEFT) {
        tutData.player.shiftX -= 100;
    }

    if (tutData.RIGHT) {
        tutData.player.shiftX += 100;
    }

    if (joystick2.deltaX() !== 0) {
        tutData.player.shiftX = joystick2.deltaX();
    }

    if (joystick2.deltaY() !== 0) {
        tutData.player.shiftY = joystick2.deltaY();
    }
    // perform player collison check operation
    let pCCResult = playerCollisionCheck(
        tut_opMap,
        tut_opMapWidth,
        tutData.player.x,
        tutData.player.y,
        tutData.player.shiftX,
        tutData.player.shiftY,
        tutData.player.speed,
        progress
    );

    // apply the changes sent back from the physics operation
    tutData.player.x = pCCResult[0];
    tutData.player.y = pCCResult[1];

    // perform the particle updates
    let pPUResult = performParticleUpdates(
        tut_opMap,
        tut_opMapWidth,
        tutData.particles,
        progress,
        tutData.mobs
    );

    // apply the changes sent back from the physics operation
    for (var p = 0; p < pPUResult.length; p++) tutData.particles[pPUResult[p]].hit();

    const mobkeys = Object.keys(tutData.mobs);

    for (var m = 0; m < mobkeys.length; m++) {
        switch (tutData.mobs[mobkeys[m]].type) {
            case MOB_TYPE_TERRIS:
                tutData.mobs[mobkeys[m]] = MobUpdateTerris(tutData.mobs[mobkeys[m]], timestamp);
                break;
            case MOB_TYPE_WINGED_TERRIS:
                tutData.mobs[mobkeys[m]] = MobUpdateWingedTerris(
                    tutData.mobs[mobkeys[m]],
                    tutData.mazeWidth,
                    progress,
                    timestamp
                );
                break;
        }
    }
});

tutView.set('render', function (canvas, ctx) {
    ctx.save();
    ctx.translate(tutData.jitterX, tutData.jitterY);
    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    // TODO optimize by caching width and height variables
    ctx.drawImage(
        tutData.mapcanvas,
        tutData.player.x - CORNER_PADX,
        tutData.player.y - CORNER_PADY,
        $(window).width(),
        $(window).height(),
        0,
        0,
        $(window).width(),
        $(window).height()
    );
    // particles
    const keys = Object.keys(tutData.particles);
    let part;
    for (var i = 0; i < keys.length; i++) {
        ctx.fillStyle = "#f00";
        part = tutData.particles[keys[i]];
        if (typeof part == "undefined") continue;

        ctx.fillRect(
            part.getX() + CORNER_PADX - tutData.player.x + (UNIT * 1.25),
            part.getY() + CORNER_PADY - tutData.player.y + (UNIT * 1.25),
            HALF_UNIT,
            HALF_UNIT
        );
    }

    // player
    ctx.fillStyle = "#0f0";
    ctx.fillRect(UNIT + CORNER_PADX, UNIT + CORNER_PADY, UNIT, UNIT);

    // mobs
    const mobkeys2 = Object.keys(tutData.mobs);
    for (var m = 0; m < mobkeys2.length; m++) {
        ctx.drawImage(
            ANIMS[tutData.mobs[mobkeys2[m]].anim].frame[tutData.mobs[mobkeys2[m]].frame],
            tutData.mobs[mobkeys2[m]].x + CORNER_PADX - tutData.player.x,
            tutData.mobs[mobkeys2[m]].y + CORNER_PADY - tutData.player.y
        );
    }

    // show mouse target
    ctx.fillStyle = "#00f";
    ctx.fillRect(tutData.mouseX, tutData.mouseY, HALF_UNIT, HALF_UNIT);

    // tutorial text
    ctx.fillStyle = "#fff";
    ctx.fillRect($(window).width() / 4, $(window).height() / 20, $(window).width() / 2, $(window).height() / 5);
    ctx.fillStyle = "#000";
    ctx.fillRect(
        ($(window).width() / 4) + UNIT / 2,
        ($(window).height() / 20) + UNIT / 2,
        ($(window).width() / 2) - UNIT,
        ($(window).height() / 5) - UNIT
    );

    ctx.fillStyle = "#fff";
    ctx.textAlign = "center";
    ctx.font = UNIT + "px Permanent Marker";
    ctx.fillText(tutData.stages[tutData.stage][0] || "", $(window).width() / 2, ($(window).height() / 20) + UNIT);
    ctx.fillText(tutData.stages[tutData.stage][1] || "", $(window).width() / 2, ($(window).height() / 20) + (2 * UNIT));
    ctx.fillText(tutData.stages[tutData.stage][2] || "", $(window).width() / 2, ($(window).height() / 20) + (3 * UNIT));
    ctx.font = UNIT / 2 + "px Orbitron";
    if (GAME_DATA.MOBILE) ctx.fillText(
        menuData.hide ? "Tap to continue" : "",
        $(window).width() / 2,
        ($(window).height() / 20) + (3.4 * UNIT)
    );

    else ctx.fillText(
        menuData.hide ? "Press <ENTER> to continue" : "",
        $(window).width() / 2,
        ($(window).height() / 20) + (4.5 * UNIT)
    );

    ctx.restore();
});
