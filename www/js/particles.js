var Particle = function(id, x, y, w, h, mX, mY, type, hit) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.prevx = 0;
    this.prevy = 0;
    this.flipX = 1;
    this.flipY = 1;
    this.corner_stone = 0;
    this.motionX = mX;
    this.motionY = mY;
    this.type = type;
    this.hit = hit;
};

Particle.prototype.getX = function() {
    return Math.round(this.x);
};

Particle.prototype.getY = function() {
    return Math.round(this.y);
};

Particle.prototype.flipX = function() {
    this.flipX = this.flipX * -1;
};

Particle.prototype.flipY = function() {
    this.flipY = this.flipY * -1;
};

Particle.prototype.invertedX = function() {
    return (this.flipX == -1);
};

Particle.prototype.invertedY = function() {
    return (this.flipY == -1);
};

Particle.prototype.update = function(progress) {
    this.x += this.motionX * this.flipX * progress;
    this.y += this.motionY * this.flipY * progress;
};
