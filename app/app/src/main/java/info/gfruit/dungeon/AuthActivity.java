package info.gfruit.dungeon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

public class AuthActivity extends AppCompatActivity {

    public static final String USER_ID = "info.gfruit.dungeon.USER_ID";
    public static final String AUTH = "info.gfruit.dungeon.AUTH";
    public static final String USER = "info.gfruit.dungeon.USER";

    private GoogleApiClient mGoogleApiClient;

    private static final int RC_SIGN_IN = 9001;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_auth);
        FirebaseApp.initializeApp(getApplicationContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            auth =  FirebaseAuth.getInstance();
            auth.getCurrentUser().getToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (task.isSuccessful()) {
                        String idToken = task.getResult().getToken();
                        try {
                            proceed(idToken);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        task.getException();
                    }
                }
            });
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()
                            )).build(), RC_SIGN_IN);
        }
    }

    private void proceed(String token) throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("idtoken", token);
        final AuthActivity parent = this;
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, "http://gfruit.info:8080/gcallback", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Intent intent = new Intent(parent, MainActivity.class);
                    Crashlytics.setUserIdentifier(response.getString("userid"));
                    Crashlytics.setUserEmail(auth.getCurrentUser().getEmail());
                    Crashlytics.setUserName(auth.getCurrentUser().getDisplayName());
                    intent.putExtra(AuthActivity.USER_ID, response.getString("userid"));
                    intent.putExtra(AuthActivity.AUTH, response.getString("auth"));
                    intent.putExtra(AuthActivity.USER, response.getString("user"));
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Auth", "Error with /gcallback: " + error.getMessage());
                Toast.makeText(parent, "Error authenticating with dungeon. Please try again.", Toast.LENGTH_LONG);
            }
        });

        queue.add(jsonRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            // Successfully signed in
            if (resultCode == ResultCodes.OK) {
                IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
                Log.d("Auth", "Successfully logged in to account");
                FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
                mUser.getToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {
                            try {
                                proceed(task.getResult().getToken());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.d("Auth", String.valueOf(task.getException()));
                        }
                    }
                });

                return;
            } else {
                // Sign in failed
                Log.d("Auth", "Failed to logged in to account");
                if (response == null) {
                    // User pressed back button
                    // TODO handle
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    // TODO handle
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    // TODO handle
                    return;
                }
            }

            // TODO handle
        }
    }
}
