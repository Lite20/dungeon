package info.gfruit.dungeon;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements RewardedVideoAdListener {

    private static final int SHOW_ADD = 1203;

    public WebView gameView;

    private RewardedVideoAd mAd;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, "ca-app-pub-8954687444503152~5839162628");
        mAd = MobileAds.getRewardedVideoAdInstance(this);
        mAd.setRewardedVideoAdListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message inputMessage) {
                int req = (int) inputMessage.obj;
                switch(req) {
                    case SHOW_ADD:
                        break;
                }
            }
        };

        Intent intent = getIntent();
        String user_id = intent.getStringExtra(AuthActivity.USER_ID);
        String auth = intent.getStringExtra(AuthActivity.AUTH);
        String user = null;
        try {
            user = new JSONObject(intent.getStringExtra(AuthActivity.USER)).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        gameView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = gameView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        gameView.addJavascriptInterface(new GameExtension(), "platform");

        final Context context = getApplicationContext();
        gameView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast toast = Toast.makeText(context, "[" + errorCode + "] " + description + " @ " + failingUrl, Toast.LENGTH_LONG);
                toast.show();
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });

        String lang = Locale.getDefault().getLanguage();
        gameView.loadUrl("file:///android_asset/www/index.html?userid=" + user_id + "&auth=" + auth + "&user=" + user + "&lang=" + lang);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        gameView.loadUrl("javascript:onResume()");
        Log.d("main", "RESUMED");
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        gameView.loadUrl("javascript:onPause()");
        Log.d("main", "PAUSED");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gameView.destroy();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        if (mAd.isLoaded()) {
            mAd.show();
        }
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }

    public class GameExtension {
        GameExtension() {}

        @JavascriptInterface
        public boolean android() {
            return true;
        }

        @JavascriptInterface
        public void loadRewardedVideoAd() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAd.loadAd("ca-app-pub-8954687444503152/7315895820", new AdRequest.Builder().addTestDevice("5EA35E4E5ECE36669AD6EA5DDBDAAC86").build());
                }
            });
        }
    }
}
