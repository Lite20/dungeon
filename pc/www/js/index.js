// http://gfruit.info:8080/getauth?medium=discord
if (window.localStorage.getItem("userid") == null) {
    if (!getParameterByName('userid')) {
        let root = window.location.href;
        let url = 'http://' + BASE_URL + '/auth.html?root=' + encodeURIComponent(root);
        window.location.href = url;
    } else {
        window.localStorage.setItem("userid", getParameterByName('userid'));
        window.localStorage.setItem("auth", getParameterByName('auth'));
        window.localStorage.setItem("user", getParameterByName('user'));
    }
}

if (JSON.parse(window.localStorage.getItem('user')) !== null) {
    Raven.setUserContext({
        id: window.localStorage.getItem('userid'),
        user: JSON.parse(window.localStorage.getItem('user')).nick
    });
} else {
    Raven.setUserContext({
        id: window.localStorage.getItem('userid')
    });
}

// detect platform
detectPlatform();

// init stats counter
initStats();

// init joysticks
initJoysticks();

// init peerjs
initPeerjs();

// init canvas
initCanvas();

// init runtime loops
initLoops();

// init key input
initKeyInput();

// init websocket
initWebsocket();

// init the console
initConsole();

function onPause() {
    EVENT_HANDLER.emit('pause');
}

function onResume() {
    EVENT_HANDLER.emit('resume');
}
