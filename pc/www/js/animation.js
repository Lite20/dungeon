var Animation = function(path, w, h, frames, desiredWidth, desiredHeight, speed, cb) {
    cb = cb || function() {};
    this.frame = [];
    this.sprite_sheet = new Image();
    this.width = desiredWidth;
    this.height = desiredHeight;
    let can, context, i;
    this.sprite_sheet.addEventListener('load', () => {
        i = 0;
        for(var y = 0; y < this.sprite_sheet.height; y += h) {
            for(var x = 0; x < this.sprite_sheet.width; x += w) {
                if(i < frames) {
                    i++;
                } else {
                    break;
                }

                can = document.createElement('CANVAS');
                can.width = desiredWidth;
                can.height = desiredHeight;
                context = can.getContext('2d');
                context.imageSmoothingEnabled = false;
                context.drawImage(this.sprite_sheet, x, y, w, h, 0, 0, desiredWidth, desiredHeight);
                this.frame.push(can);
            }
        }

        cb();
    }, false);
    this.sprite_sheet.src = path;
};
