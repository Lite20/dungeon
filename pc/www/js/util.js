const drawText = function (ctx, msg, x, y, alignHor, alignVer, color, font) {
    ctx.font = font;
    ctx.fillStyle = color;
    ctx.textAlign = alignHor;
    ctx.textBaseline = alignVer;
    ctx.fillText(msg, x, y);
};

const cell_w = CELL_SIZE - (2 * UNIT);

var cellX, cellY;

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    var = results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function detectPlatform() {
    GAME_DATA.MOBILE = false;
    if (typeof platform !== "undefined")
        if (platform.android) GAME_DATA.MOBILE = true;
}

function getCellRange(x, y, w, h, col_map, map_w) {
    cellX = Math.floor(x / cell_w);
    cellY = Math.floor(y / cell_w);
    return {
        scopeX: {
            minX: cellX,
            maxX: cellX + cell_w
        },
        scopeY: {
            minY: cellY,
            maxY: cellY + cell_w
        }
    };
}

function getYRange(x, y, w, h, col_map, map_w) {
    let minY = y;
    let maxY = y;

    // top left corner overlaps wall
    while (col_map[Math.round(x + (minY * map_w))] == 0) minY -= h;

    // we go over by 1 so we must undo it
    minY += h;

    // top left corner overlaps wall
    while (col_map[Math.round(x + ((maxY + h) * map_w))] == 0) maxY += h;

    // we go over by 1 so we must undo it
    maxY -= h;

    return {
        minY: minY,
        maxY: maxY
    };
}

function getXRange(x, y, w, h, col_map, map_w) {
    let minX = x;
    let maxX = x;

    // top left corner overlaps wall
    while (col_map[Math.round(minX + (y * map_w))] == 0) minX -= w;

    // we go over by 1 so we must undo it
    minX += w;

    // top left corner overlaps wall
    while (col_map[Math.round(maxX + w + (y * map_w))] == 0) maxX += w;

    // we go over by 1 so we must undo it
    maxX -= w;

    return {
        minX: minX,
        maxX: maxX
    }
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function optimizeCollsionMap(canv) {
    var map = [];
    var ctxx = canv.getContext('2d');
    var imgD = ctxx.getImageData(0, 0, canv.width, canv.height);

    for (var i = 0; i < imgD.data.length; i += 4) map.push(imgD.data[i]);

    console.log("from " + imgD.data.length + " to " + map.length);

    return {
        map: map,
        w: canv.width
    };
}

function getRomanNumeral(num) {
    let s, b, a, o, t;
    t = num / 1e3 | 0;
    num %= 1e3;
    for (s = b = '', a = 5; num; b++, a ^= 7)
        for (o = num % a, num = num / a ^ 0; o--;) s = 'IVXLCDM'.charAt(o > 2 ? b + num - (num &= ~1) + (o = 1) : b) + s;

    return Array(t + 1).join('M') + s;
}

function setQuality(qual) {
    if (quality == "high") {
        canvas.width = $(window).width() * 2 - 5;
        canvas.height = $(window).height() * 2 - 5;
    } else {
        canvas.width = $(window).width() - 5;
        canvas.height = $(window).height() - 5;
    }
}

function getRandomInt(rand, min, max) {
    return Math.floor(rand() * (max - min + 1)) + min;
}

function renderMaze(maze, maze_size) {
    let canva = document.createElement("CANVAS");
    let mapctx = canva.getContext('2d');
    canva.width = CELL_SIZE * maze_size;
    canva.height = CELL_SIZE * maze_size;
    for (var y = 0; y < maze_size; y++) {
        for (var x = 0; x < maze_size; x++) {
            mapctx.beginPath();
            mapctx.lineWidth = UNIT;
            mapctx.strokeStyle = "#ffffff";
            mapctx.rect(Math.round(x * CELL_SIZE + UNIT), Math.round(y * CELL_SIZE + UNIT), Math.round(CELL_SIZE * 0.8), Math.round(CELL_SIZE * 0.8));
            mapctx.stroke();
        }
    }

    let UP, DOWN, LEFT, RIGHT;
    for (var y = 0; y < maze_size; y++) {
        for (var x = 0; x < maze_size; x++) {
            UP = false;
            DOWN = false;
            LEFT = false;
            RIGHT = false;
            if (typeof maze.map[y][x] == "object") {
                for (var i = 0; i < maze.map[y][x].length; i++) {
                    switch (maze.map[y][x][i]) {
                        case maze.UP:
                            UP = true;
                            break;

                        case maze.DOWN:
                            DOWN = true;
                            break;

                        case maze.LEFT:
                            LEFT = true;
                            break;

                        case maze.RIGHT:
                            RIGHT = true;
                            break;
                    }
                }
            } else {
                switch (maze.map[y][x]) {
                    case maze.UP:
                        UP = true;
                        break;

                    case maze.DOWN:
                        DOWN = true;
                        break;

                    case maze.LEFT:
                        LEFT = true;
                        break;

                    case maze.RIGHT:
                        RIGHT = true;
                        break;
                    default:
                        // TODO Rage in error (╯°□°）╯︵ ┻━┻ (It's unset)
                        break;
                }
            }

            mapctx.fillStyle = "#fff";
            if (UP) {
                mapctx.fillRect(
                    Math.round((x * CELL_SIZE) + (CELL_SIZE * 0.25)),
                    Math.round((y * CELL_SIZE) - (CELL_SIZE * 0.15)),
                    Math.round((CELL_SIZE * 0.5)),
                    Math.round(CELL_SIZE * 0.3)
                );

                mapctx.clearRect(
                    Math.round((x * CELL_SIZE) + (CELL_SIZE * 0.35)),
                    Math.round((y * CELL_SIZE) - (CELL_SIZE * 0.15) - 2),
                    Math.round((CELL_SIZE * 0.3)),
                    Math.round((CELL_SIZE * 0.3) + 4)
                );
            }

            if (DOWN) {
                mapctx.fillRect(
                    Math.round((x * CELL_SIZE) + (CELL_SIZE * 0.25)),
                    Math.round((y * CELL_SIZE) - UNIT + (CELL_SIZE * 0.95)),
                    Math.round((CELL_SIZE * 0.5)),
                    Math.round(CELL_SIZE * 0.3)
                );

                mapctx.clearRect(
                    Math.round((x * CELL_SIZE) + (CELL_SIZE * 0.35)),
                    Math.round((y * CELL_SIZE) - UNIT + (CELL_SIZE * 0.95) - 2),
                    Math.round((CELL_SIZE * 0.3)),
                    Math.round((CELL_SIZE * 0.3) + 4)
                );
            }

            if (LEFT) {
                mapctx.fillRect(
                    Math.round((x * CELL_SIZE) - (CELL_SIZE * 0.15)),
                    Math.round((y * CELL_SIZE) + (CELL_SIZE * 0.25)),
                    Math.round(CELL_SIZE * 0.3),
                    Math.round(CELL_SIZE * 0.5)
                );

                mapctx.clearRect(
                    Math.round((x * CELL_SIZE) - (CELL_SIZE * 0.15) - 2),
                    Math.round((y * CELL_SIZE) + (CELL_SIZE * 0.35)),
                    Math.round(CELL_SIZE * 0.3 + 4),
                    Math.round(CELL_SIZE * 0.3)
                );
            }

            if (RIGHT) {
                mapctx.fillRect(
                    Math.round((x * CELL_SIZE) + (CELL_SIZE * 0.85)),
                    Math.round((y * CELL_SIZE) + (CELL_SIZE * 0.25)),
                    Math.round(CELL_SIZE * 0.3),
                    Math.round(CELL_SIZE * 0.5)
                );

                mapctx.clearRect(
                    Math.round((x * CELL_SIZE) + (CELL_SIZE * 0.85) - 2),
                    Math.round((y * CELL_SIZE) + (CELL_SIZE * 0.35)),
                    Math.round(CELL_SIZE * 0.3 + 4),
                    Math.round(CELL_SIZE * 0.3)
                );
            }
        }
    }

    return canva;
}

var Maze = function (scale, seed) {
    if (seed) this.rand = new Math.seedrandom(seed);
    else this.rand = new Math.seedrandom();

    this.generate(scale);
};

Maze.prototype.UNSET = DEBUG_MODE ? '*' : 0;
Maze.prototype.END = DEBUG_MODE ? '●' : 0;
Maze.prototype.SPLIT = DEBUG_MODE ? '○' : 0;
Maze.prototype.UP = DEBUG_MODE ? '↑' : 1;
Maze.prototype.DOWN = DEBUG_MODE ? '↓' : 2;
Maze.prototype.LEFT = DEBUG_MODE ? '←' : 3;
Maze.prototype.RIGHT = DEBUG_MODE ? '→' : 4;
Maze.prototype.generate = function (scale) {
    this.map = [];

    // init the map
    for (var i = 0; i < scale; i++) {
        this.map[i] = [];
        for (var x = 0; x < scale; x++) this.map[i][x] = this.UNSET;
    }

    let current = {
        x: getRandomInt(this.rand, 0, scale - 1),
        y: getRandomInt(this.rand, 0, scale - 1)
    };

    var opts, chosen, stack = [];
    this.map[current.y][current.x] = this.END;
    for (var i = 1; i < (scale * scale); i++) {
        if (stack.length == 0 && i == (scale * scale) - 1) break;
        opts = this.getOptions(current.y, current.x);
        // TODO simplify this and have all elements in arrays instead of just splits
        if (opts.length > 0) {
            chosen = getRandomInt(this.rand, 0, opts.length - 1);
            if (this.map[current.y][current.x] == this.UNSET) this.map[current.y][current.x] = opts[chosen];
            else {
                if (this.map[current.y][current.x].length > 1) {
                    this.map[current.y][current.x].push(opts[chosen]);
                } else {
                    this.map[current.y][current.x] = [this.map[current.y][current.x], opts[chosen]];
                }
            }

            stack.push(current);
            current = this.apply(opts[chosen], current);
        } else {
            this.map[current.y][current.x] = this.END;
            while (opts.length == 0) {
                stack.splice(-1);
                if (stack.length == 0) break;
                current = stack[stack.length - 1];
                opts = this.getOptions(current.y, current.x);
            }

            current = stack[stack.length - 1];
            i--;
        }
    }
};

Maze.prototype.apply = function (opt, current) {
    if (opt == this.UP) {
        return {
            x: current.x,
            y: current.y - 1
        };
    } else if (opt == this.DOWN) {
        return {
            x: current.x,
            y: current.y + 1
        };
    } else if (opt == this.LEFT) {
        return {
            x: current.x - 1,
            y: current.y
        };
    } else if (opt == this.RIGHT) {
        return {
            x: current.x + 1,
            y: current.y
        };
    }
};

Maze.prototype.getOptions = function (y, x) {
    var opts = [];
    if (this.map[y - 1])
        if (this.map[y - 1][x] == this.UNSET) opts.push(this.UP);

    if (this.map[y + 1])
        if (this.map[y + 1][x] == this.UNSET) opts.push(this.DOWN);

    if (this.map[y])
        if (this.map[y][x - 1] == this.UNSET) opts.push(this.LEFT);

    if (this.map[y])
        if (this.map[y][x + 1] == this.UNSET) opts.push(this.RIGHT);

    return opts;
};
