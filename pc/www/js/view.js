var views = {};
var view = "boot";
var View = function(tag) {
    this.tag = tag;
    physicStore[tag] = {
        hiatus: false
    };
    
    views[tag] = {};
};

View.prototype.set = function(property, val) {
    views[this.tag][property] = val;
};

View.prototype.showView = function(target) {
    if (views[target]) {
        view = target;
        views[view].init();
    } else {
        console.log("Attempting to go to non-existent view " + target);
    }
};
