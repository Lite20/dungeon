var menuView = new View("menu");
var menuData = {
    hide: false
};

GAME_DATA.MUSIC.menuBackground = new Howl({
    src: ['assets/audio/amethyst - menu.mp3'],
    autoplay: false,
    loop: false
});

GAME_DATA.MUSIC.menuAltBackground = new Howl({
    src: ['assets/audio/amethyst - alt_intro.mp3'],
    autoplay: false,
    loop: false
});

GAME_DATA.MUSIC.posPing = new Howl({
    src: ['assets/audio/dopplo_pos_ping.mp3']
});

// to prevent errors
menuView.set('fire_click', function(e) {});
menuView.set('mousemove', function(e) {});
menuView.set('keypress', function(e) {});
menuView.set('keyup', function(e) {});
menuView.set('peer_con', function(e) {});
menuView.set('ws_message', function(e) {});
menuView.set('init', function() {
    switch(Math.round(Math.random() * 3)) {
        case 1:
            menuData.selectedBackground = "menuBackground";
            break;
        case 2:
            menuData.selectedBackground = "menuAltBackground";
            break;
        default:
            menuData.selectedBackground = "menuBackground";
            break;
    }

    GAME_DATA.MUSIC[menuData.selectedBackground].play();
});

menuView.set('pause', function() {
    GAME_DATA.MUSIC[menuData.selectedBackground].pause();
});

menuView.set('resume', function() {
    GAME_DATA.MUSIC[menuData.selectedBackground].play();
});

let click_handle = function() {
    GAME_DATA.MUSIC.posPing.play();
    menuView.showView("lobby");
};

menuView.set('click', click_handle);
menuView.set('mouse_click', click_handle);
menuView.set('update', function(progress) {});
menuView.set('shot_click', function(e) {});
menuView.set('render', function(canvas, ctx) {
    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    drawText(
        ctx,
        "Dungeon", (canvas.width / 2), (canvas.height / 2),
        "center",
        "bottom", "#fff", (canvas.height / 8) + "px Permanent Marker"
    );

    if (!menuData.hide) {
        drawText(
            ctx,
            "t a p   t o   p l a y", (canvas.width / 2), (canvas.height / 2),
            "center",
            "top", "#fff", (canvas.height / 25) + "px Orbitron"
        );
    }
});

menuData.flash = setInterval(function() {
    menuData.hide = !menuData.hide;
}, 500);
