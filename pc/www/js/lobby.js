var lobbyView = new View("lobby");
var lobbyData = {
    jitterForce: 0,
    jitterX: 0,
    jitterY: 0,
    maze: new Maze(MAZE_SIZE),
    UP: false,
    DOWN: false,
    LEFT: false,
    RIGHT: false,
    shotsPerSecond: 3,
    mouseX: 0,
    mouseY: 0,
    player: {
        x: 26,
        y: 26,
        speed: 2,
        life: 100,
        shiftX: 0,
        shiftY: 0
    },
    particles: {}
};

lobbyData.mapcanvas = renderMaze(lobbyData.maze, MAZE_SIZE);
lobbyData.map_width = lobbyData.mapcanvas.width * 4;
let oppedMap = optimizeCollsionMap(lobbyData.mapcanvas);
const lobby_opMap = oppedMap.map;
const lobby_opMapWidth = oppedMap.w;
lobbyView.set('fire_click', function(e) {});
lobbyView.set('shot_click', function(e) {});
lobbyView.set('mousemove', function(e) {});
lobbyView.set('peer_con', function(e) {});
lobbyView.set('mousemove', function(e) {
    lobbyData.mouseX = e.clientX;
    lobbyData.mouseY = e.clientY;
});

let id;
lobbyView.set('keypress', function(e) {
    switch (e.keyCode) {
        case 87:
            lobbyData.UP = true;
            break;
        case 65:
            lobbyData.LEFT = true;
            break;
        case 83:
            lobbyData.DOWN = true;
            break;
        case 68:
            lobbyData.RIGHT = true;
            break;
        case 37:
            id = guid();
            lobbyData.particles[id] = new Particle(
                id,
                lobbyData.player.x,
                lobbyData.player.y,
                HALF_UNIT,
                HALF_UNIT,
                -2, 0,
                function() {
                    delete lobbyData.particles[this.id];
                }
            );

            break;
        case 38:
            id = guid();
            lobbyData.particles[id] = new Particle(
                id,
                lobbyData.player.x,
                lobbyData.player.y,
                HALF_UNIT,
                HALF_UNIT,
                0, -2,
                function() {
                    delete lobbyData.particles[this.id];
                }
            );

            break;
        case 39:
            id = guid();
            lobbyData.particles[id] = new Particle(
                id,
                lobbyData.player.x,
                lobbyData.player.y,
                HALF_UNIT,
                HALF_UNIT,
                2, 0,
                function() {
                    delete lobbyData.particles[this.id];
                }
            );

            break;
        case 40:
            id = guid();
            lobbyData.particles[id] = new Particle(
                id,
                lobbyData.player.x,
                lobbyData.player.y,
                HALF_UNIT,
                HALF_UNIT,
                0, 2,
                function() {
                    delete lobbyData.particles[this.id];
                }
            );

            break;
        default:
            console.log(e.keyCode);
            break;
    }
});

lobbyView.set('keyup', function(e) {
    switch (e.keyCode) {
        case 87:
            lobbyData.UP = false;
            break;
        case 65:
            lobbyData.LEFT = false;
            break;
        case 83:
            lobbyData.DOWN = false;
            break;
        case 68:
            lobbyData.RIGHT = false;
            break;
        default:
            console.log(e.keyCode);
            break;
    }
});

lobbyView.set('click', function(e) {

});

var lobby_guid2;
var lobby_hiatus = false;
// TODO improve accuracy
lobbyView.set('mouse_click', function(e) {
    var x = e.clientX - (canvas.width / 2);
    var y = e.clientY - (canvas.height / 2);
    if (x > 100) {
        let oldx = x;
        x = 100;
        y = y * (100 / oldx);
    }

    if (x < -100) {
        let oldx = x;
        x = -100;
        y = y * (-100 / oldx);
    }

    if (y > 100) {
        let oldy = y;
        y = 100;
        x = x * (100 / oldy);
    }

    if (y < -100) {
        let oldy = y;
        y = -100;
        x = x * (-100 / oldy);
    }

    if (!lobby_hiatus) {
        lobby_hiatus = true;
        setTimeout(function() {
            lobby_hiatus = false;
        }, 1000 / lobbyData.shotsPerSecond);
        lobby_guid2 = guid();
        lobbyData.particles[lobby_guid2] = new Particle(
            lobby_guid2,
            lobbyData.player.x,
            lobbyData.player.y,
            HALF_UNIT,
            HALF_UNIT,
            x / 50,
            y / 50,
            function() {
                delete lobbyData.particles[this.id];
            }
        );
    }
});

lobbyView.set('pause', function() {
    GAME_DATA.MUSIC[menuData.selectedBackground].pause();
});

lobbyView.set('resume', function() {
    GAME_DATA.MUSIC[menuData.selectedBackground].play();
});

lobbyView.set('ws_message', function(msg) {
    let data = JSON.parse(msg);
    if (data.id == 'identify') {
        if (data.s !== 'suc') {
            console.log("failed to auth with gameapi");
        } else {
            GAME_DATA.queue = data.queue;
            console.log("authed with gameapi");
        }
    } else if (data.id == 'broadcast') {
        GAME_DATA.vote += data.vote;
        for (var i = 0; i < data.buffer.length; i++) {
            if (data.buffer[i].id == 'join') {
                GAME_DATA.queue.push(data.buffer[i].peerid);
                break;
            }
        }
    }
});

lobbyView.set('init', function() {
    GAME_DATA.queue = [];
    GAME_DATA.vote = 0;
    GAME_DATA.socket.send(JSON.stringify({
        r: 'identify',
        id: window.localStorage.getItem("userid"),
        auth: window.localStorage.getItem("auth"),
        peerid: GAME_DATA.PEER_ID
    }));

    if (window.localStorage.getItem("did_tutorial") == null) {
        // window.localStorage.setItem("did_tutorial", true);
        lobbyView.showView("tutorial");
    }
});

let lobby_prevx, lobby_prevy, lobby_guid;
let lobby_mapctx = lobbyData.mapcanvas.getContext('2d');
let lobby_tick = 0;
lobbyView.set('update', function(progress, timestamp) {
    lobbyData.jitterX = Math.round(Math.random() * tutData.jitterForce);
    lobbyData.jitterY = Math.round(Math.random() * tutData.jitterForce);
    lobby_tick++;
    if (lobby_tick > 1) {
        lobby_tick = 0;
    }
    // TODO only do if mobile
    // perform particle creation operation for mobile devices
    let mPGOResult = mobileParticleGenOp(
        "lobby",
        joystick.deltaX(),
        joystick.deltaY(),
        lobbyData.player.x,
        lobbyData.player.y,
        lobbyData.shotsPerSecond
    );
    // iterate over result creating particles requested by the physics operation
    for (var i = 0; i < mPGOResult.length; i++) {
        mPGOResult[i].hit = function() {
            delete lobbyData.particles[this.id];
        };

        lobbyData.particles[mPGOResult[i].id] = mPGOResult[i];
    }
    // check if the player moved in any direction
    lobbyData.player.shiftY = 0;
    lobbyData.player.shiftX = 0;
    if (lobbyData.UP) {
        lobbyData.player.shiftY -= 100;
    }

    if (lobbyData.DOWN) {
        lobbyData.player.shiftY += 100;
    }

    if (lobbyData.LEFT) {
        lobbyData.player.shiftX -= 100;
    }

    if (lobbyData.RIGHT) {
        lobbyData.player.shiftX += 100;
    }

    if (joystick2.deltaX() !== 0) {
        lobbyData.player.shiftX = joystick2.deltaX();
    }

    if (joystick2.deltaY() !== 0) {
        lobbyData.player.shiftY = joystick2.deltaY();
    }
    // perform player collison check operation
    let pCCResult = playerCollisionCheck(
        lobby_opMap,
        lobby_opMapWidth,
        lobbyData.player.x,
        lobbyData.player.y,
        lobbyData.player.shiftX,
        lobbyData.player.shiftY,
        lobbyData.player.speed,
        progress
    );
    // apply the changes sent back from the physics operation
    lobbyData.player.x = pCCResult[0];
    lobbyData.player.y = pCCResult[1];
    // perform the particle updates
    let pPUResult = performParticleUpdates(
        lobby_opMap,
        lobby_opMapWidth,
        lobbyData.particles,
        progress
    );

    for (var p = 0; p < pPUResult.length; p++) {
        // apply the changes sent back from the physics operation
        lobbyData.particles[pPUResult[p]].hit();
    }
});

lobbyView.set('render', function(canvas, ctx) {
    ctx.save();
    ctx.translate(lobbyData.jitterX, lobbyData.jitterY);
    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    // TODO optimize by caching width and height variables
    ctx.drawImage(
        lobbyData.mapcanvas,
        lobbyData.player.x - CORNER_PADX,
        lobbyData.player.y - CORNER_PADY,
        $(window).width(),
        $(window).height(),
        0,
        0,
        $(window).width(),
        $(window).height()
    );
    // particles
    const keys = Object.keys(lobbyData.particles);
    let part;
    for (var i = 0; i < keys.length; i++) {
        ctx.fillStyle = "#f00";
        part = lobbyData.particles[keys[i]];
        if (typeof part == "undefined") {
            continue;
        }

        ctx.fillRect(
            part.getX() + CORNER_PADX - lobbyData.player.x + (UNIT * 1.25),
            part.getY() + CORNER_PADY - lobbyData.player.y + (UNIT * 1.25),
            HALF_UNIT,
            HALF_UNIT
        );
    }
    // player
    ctx.fillStyle = "#0f0";
    ctx.fillRect(UNIT + CORNER_PADX, UNIT + CORNER_PADY, UNIT, UNIT);
    // show mouse target
    ctx.fillStyle = "#00f";
    ctx.fillRect(lobbyData.mouseX, lobbyData.mouseY, HALF_UNIT, HALF_UNIT);
    // loading
    ctx.fillStyle = "#f00";
    ctx.fillRect(0, 0, canvas.width, canvas.height / 25);
    drawText(ctx, "loading...", (canvas.width / 2), 0,
        "center",
        "top", "#fff", (canvas.height / 30) + "px Orbitron"
    );

    ctx.restore();
});
