const ANIMS = {
    MOB_TERRIS: new Animation("assets/graphics/mobs/mob_terris.png", 65, 65, 8, Math.round(UNIT * 1.5), Math.round(UNIT * 1.5)),
    MOB_WINGED_TERRIS: new Animation("assets/graphics/mobs/mob_winged_terris.png", 250, 110, 4, Math.round(UNIT * 5.5), Math.round(UNIT * 2.5))
};

function MobCreateTerris(id, x, y, col_map, map_w, hit, seed) {
    console.log(arguments);
    let terris = {};
    terris.id = id;
    terris.seed = seed;
    terris.x = x;
    terris.y = y;
    terris.hit = hit;
    terris.type = MOB_TYPE_TERRIS;
    terris.dir = -1;
    terris.frame = 0;
    terris.anim = "MOB_TERRIS";
    terris.w = ANIMS.MOB_TERRIS.width;
    terris.h = ANIMS.MOB_TERRIS.height;
    terris.wait = Math.ceil((1 + Math.random()) * 100) * 3;
    terris.scope = getXRange(x, y, terris.w, terris.h, col_map, map_w);
    return terris;
}

function MobCreateWingedTerris(id, x, y, col_map, map_w, hit, seed) {
    let winged_terris = {};
    winged_terris.id = id;
    winged_terris.seed = seed;
    winged_terris.x = x;
    winged_terris.y = y;
    winged_terris.hit = hit;
    winged_terris.type = MOB_TYPE_WINGED_TERRIS;
    winged_terris.dirX = -1;
    winged_terris.dirY = -1;
    winged_terris.frame = 0;
    winged_terris.anim = "MOB_WINGED_TERRIS";
    winged_terris.w = ANIMS.MOB_WINGED_TERRIS.width;
    winged_terris.h = ANIMS.MOB_WINGED_TERRIS.height;
    winged_terris.speedX = Math.ceil(Math.random() * 4);
    winged_terris.speedY = Math.ceil(Math.random() * 4);
    return winged_terris;
}

function MobUpdateTerris(terris, timestamp) {
    if (!terris.last_timestamp) {
        terris.last_timestamp = timestamp;
    }

    while (timestamp - terris.last_timestamp > terris.wait) {
        terris.last_timestamp += terris.wait;
        terris.x += (terris.w * 0.5) * terris.dir;
        if (terris.x < terris.scope.minX) {
            terris.x += (terris.w * 0.5) * terris.dir;
            terris.dir = terris.dir * -1;
            terris.x += (terris.w * 0.5) * terris.dir
        } else if (terris.x > terris.scope.maxX) {
            terris.x -= (terris.w * 0.5) * terris.dir;
            terris.dir = terris.dir * -1;
            terris.x -= (terris.w * 0.5) * terris.dir;
        }

        terris.frame -= terris.dir;
        if (terris.frame == ANIMS.MOB_TERRIS.frame.length) {
            terris.frame = 0;
        } else if (terris.frame < 0) {
            terris.frame = ANIMS.MOB_TERRIS.frame.length - 1;
        }
    }

    return terris;
}

function MobUpdateWingedTerris(winged_terris, maze_w, progress, timestamp) {
    if(!winged_terris.last_timestamp) {
        winged_terris.last_timestamp = timestamp;
    }
    // skip to the correct frame
    while(timestamp - winged_terris.last_timestamp > 250) {
        winged_terris.last_timestamp += 250;
        winged_terris.frame++;
        if(winged_terris.frame == ANIMS.MOB_WINGED_TERRIS.frame.length) {
            winged_terris.frame = 0;
        }
    }

    winged_terris.x += winged_terris.speedX * winged_terris.dirX * progress;
    winged_terris.y += winged_terris.speedY * winged_terris.dirY * progress;
    if(winged_terris.x < 0 || winged_terris.x + winged_terris.w > maze_w) {
        winged_terris.dirX = winged_terris.dirX * -1;
    }

    if(winged_terris.y < 0 || winged_terris.y + winged_terris.h > maze_w) {
        winged_terris.dirY = winged_terris.dirY * -1;
    }

    return winged_terris;
}
